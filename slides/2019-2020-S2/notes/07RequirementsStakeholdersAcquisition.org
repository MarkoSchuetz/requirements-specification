* how do requirements stakeholders differ from domain stakeholders?


* how is requirements acquisition done in the presence of a domain description?
** which domain phenomena need to be part of requirements?
** how to make them part?


* how is requirements acquisition done in the absence of a domain description?


* what are the guiding questions to acquire machine requirements?  
** considering domain requirements
*** which degree of performance is needed?
*** which degree of dependability is needed?
*** what kind of maintenance is expected?
*** what kind of platform is expected?
*** what kind of documentation is expected?


* what are the guiding questions to acquire interface requirements?
** considering phenomena shared between the domain and the system: 
*** what shared data initialization requirements are expected?
*** what shared data update requirements are expected?
*** what computational data and control requirements are needed?
*** what shared data update requirements are expected?
*** what man-machine and machine-machine dialog requirements are expected?
*** what man-machine physiological interface requirements are expected?
